def get_seconds_format(hms):
    hms = hms.split(':')
    return int(hms[0]) * 3600 + int(hms[1]) * 60 + int(hms[2]) if len(hms) == 3 else int(hms[0]) * 60 + int(hms[1])


def get_hms_format(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    return "%d:%02d:%02d" % (h, m, s)
