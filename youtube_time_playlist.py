import time
import matplotlib.pyplot as plt
import numpy as np
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time_utilities import get_seconds_format, get_hms_format

episodes_time = []


def get_episodes_from_youtube():
    base_link = "https://www.youtube.com/playlist?list="
    playlist_id = "PLZHQObOWTQDMsr9K-rj53DwVRMYO3t5Yr"
    class_of_chapter_time = "style-scope ytd-thumbnail-overlay-time-status-renderer"
    pause_time = 0.5
    path = "C:\\Users\\Coolig\\Desktop\\ChromeDriver\\chromedriver.exe"
    browser = webdriver.Chrome(path)
    browser.get(base_link + playlist_id)
    time.sleep(pause_time)
    browser.find_element_by_tag_name('html').send_keys(Keys.END)  # scroll to end
    time.sleep(pause_time)
    spans = browser.find_elements_by_xpath('.//span[@class = "' + class_of_chapter_time + '"]')
    for element in spans:
        episodes_time.append(get_seconds_format(element.text))
    browser.quit()


def create_plot_of_episodes():
    fig, (ax, ax2) = plt.subplots(1, 2)
    index = np.arange(len(episodes_time))
    colors = ('b', 'c', 'r', 'y')
    speeds = (1, 1.2, 1.5, 2)
    episodes_times = [create_playlist(x) for x in speeds]
    for x in range(0, len(episodes_times)):
        ax.bar(index, episodes_times[x], width=0.5, alpha=1, color=colors[x], label=speeds[x])
    ax.set_xticks(index)
    ax.set_xticklabels(list(range(1, len(episodes_time) + 1)))
    ax.plot([], [], ' ', label="Speed")
    ax.legend()
    ax.set_xlabel('episodes')
    ax.set_ylabel('time episodes')
    ax.set_title('Playlist')
    fig.canvas.draw()
    ax.set_yticklabels([get_hms_format(int(i.get_text())) for i in ax.get_yticklabels()])  # change seconds to h:m:s
    cell_text = [get_playlist_full_time(x) for x in episodes_times]
    ax2.axis('tight')
    ax2.axis("off")
    ax2.table(cellText=[[speeds[x], cell_text[x]] for x in range(0, len(speeds))],
              rowLabels=[x for x in range(0, len(speeds))],
              colLabels=['speed', 'full time'], cellLoc='center', loc='center')
    plt.show()


def create_playlist(speed):
    return [int(x * 1 / speed) for x in episodes_time]


def get_playlist_full_time(times):
    full_time = 0
    for element in times:
        full_time += element
    full_time = get_hms_format(full_time)
    return str(full_time)


get_episodes_from_youtube()
create_plot_of_episodes()
