import time_utilities as t


def test_get_seconds_format():
    assert t.get_seconds_format('1:00') == 60
    assert t.get_seconds_format('2:02') == 122
    assert t.get_seconds_format('10:00:00') == 36000


def test_get_hms_format():
    assert t.get_hms_format(60) == '0:01:00'
    assert t.get_hms_format(36000) == '10:00:00'
